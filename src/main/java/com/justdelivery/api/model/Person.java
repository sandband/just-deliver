package com.justdelivery.api.model;

import java.util.Date;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@XmlRootElement
@Document
public class Person {
	
	@Id
	private String id;
	
	@NotNull
	@Pattern(message = "invalid email address", regexp = "[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}")
	private String email;
	
	private String password;
	
	@NotNull
	private String firstName;
	
	@NotNull
	private String lastName;
	
	@NotNull
	@Pattern(message = "invalid mobile number", regexp="^(\\+44\\s?7\\d{3}|\\(?07\\d{3}\\)?)\\s?\\d{3}\\s?\\d{3}$")
	private String mobileNumber;
	
	private String line1;
	
	private String line2;

	private String town;
	
	@Pattern(regexp = "(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})", message = "invalid postcode")
	private String postCode;

	private String country;
	
	private Boolean activeAccount;
	
	private Date registeredDate = new Date();
	
	private Location currentLocation;
	
	@DBRef
	private Set<GPSDevice> gpsDevices;

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getId() {
		return id;
	}
	public Date getRegisteredDate() {
		return registeredDate;
	}
	public Set<GPSDevice> getGpsDevices() {
		return gpsDevices;
	}
	public void setGpsDevices(Set<GPSDevice> gpsDevices) {
		this.gpsDevices = gpsDevices;
	}
	public String getEmail() {
	    return email;
	}
	public void setEmail(String email) {
	    this.email = email;
	}
	public String getPassword() {
	    return password;
	}
	public void setPassword(String password) {
	    this.password = password;
	}
	public Boolean getActiveAccount() {
	    return activeAccount;
	}
	public void setActiveAccount(Boolean activeAccount) {
	    this.activeAccount = activeAccount;
	}
	
	@Override
	public int hashCode() {
	    	if(getId()==null) return 17*31;
	    	
		return getId().hashCode();
	}

	@Override
	public String toString() {
		return String.format("%s %s", firstName, lastName);
	}
	public Location getCurrentLocation() {
	    return currentLocation;
	}
	public void setCurrentLocation(Location currentLocation) {
	    this.currentLocation = currentLocation;
	}
	public String getLine1() {
	    return line1;
	}
	public void setLine1(String line1) {
	    this.line1 = line1;
	}
	public String getLine2() {
	    return line2;
	}
	public void setLine2(String line2) {
	    this.line2 = line2;
	}
	public String getPostCode() {
	    return postCode;
	}
	public void setPostCode(String postCode) {
	    this.postCode = postCode;
	}
	public String getCountry() {
	    return country;
	}
	public void setCountry(String country) {
	    this.country = country;
	}
	public void setRegisteredDate(Date registeredDate) {
	    this.registeredDate = registeredDate;
	}
	public String getTown() {
	    return town;
	}
	public void setTown(String town) {
	    this.town = town;
	}
	public String getMobileNumber() {
	    return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
	    this.mobileNumber = mobileNumber;
	}

}
