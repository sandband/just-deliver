package com.justdelivery.api.model;

import javax.validation.constraints.Pattern;

public class BusinessUnit {
    
	private String businessName;
	
	private String line1;
	
	private String line2;
	
	private String town;
	
	@Pattern(regexp = "(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})", message = "invalid postcode")
	private String postCode;
	
	private String country;

	public String getBusinessName() {
	    return businessName;
	}

	public void setBusinessName(String businessName) {
	    this.businessName = businessName;
	}

	public String getLine1() {
	    return line1;
	}

	public void setLine1(String line1) {
	    this.line1 = line1;
	}

	public String getLine2() {
	    return line2;
	}

	public void setLine2(String line2) {
	    this.line2 = line2;
	}

	public String getPostCode() {
	    return postCode;
	}

	public void setPostCode(String postCode) {
	    this.postCode = postCode;
	}

	public String getCountry() {
	    return country;
	}

	public void setCountry(String country) {
	    this.country = country;
	}

	public String getTown() {
	    return town;
	}

	public void setTown(String town) {
	    this.town = town;
	}

}
