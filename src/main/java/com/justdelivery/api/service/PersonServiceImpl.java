package com.justdelivery.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.justdelivery.api.model.Person;
import com.justdelivery.api.repository.person.PersonRepository;

@Service
public class PersonServiceImpl implements PersonService<Person> {

	@Autowired
	private PersonRepository personRepository;

	@Override
	public boolean isEmailExists(Person person) {
	    if(person.getEmail() != null){
		 List<Person> matches = personRepository.findByEmail(person.getEmail());
		 if(matches.size() > 0){
		     return true;
		 }
	    }
	    return false;
	}

}
