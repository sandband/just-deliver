package com.justdelivery.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.justdelivery.api.exception.ErrorCode;
import com.justdelivery.api.exception.ErrorResponse;
import com.justdelivery.api.exception.JustDeliveryException;
import com.justdelivery.api.model.Person;
import com.justdelivery.api.repository.person.PersonRepository;

@Service
public class PersonRegistrationServiceImpl implements PersonRegistrationService<Person> {

	@Autowired
	private PersonRepository personRepository;
	
	@Override
	public String register(Person person) throws JustDeliveryException{
	    List<Person> list = personRepository.findByEmail(person.getEmail());
		if(list.size() > 0){
		    Map<String, String> fieldErrors = new HashMap<String, String>();
		    fieldErrors.put("person.email", person.getEmail());
		    
		    ErrorResponse error = new ErrorResponse(ErrorCode.EMAIL_ALREADY_EXISTS, fieldErrors);
		    error.setFieldErrors(fieldErrors);
		    
		    throw new JustDeliveryException(error);
		}
		return personRepository.save(person).getId();
	 
	}

}
