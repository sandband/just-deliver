package com.justdelivery.api.service;

import com.justdelivery.api.model.Person;

public interface PersonService <T extends Person>{
 
	public boolean isEmailExists(T person);
	 
}
