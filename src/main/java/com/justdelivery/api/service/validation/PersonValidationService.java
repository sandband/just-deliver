package com.justdelivery.api.service.validation;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.stereotype.Component;

import com.justdelivery.api.model.Person;

@Component
public class PersonValidationService {

	public ConstraintViolationException validatePersonSignUpRequest(final Person person) {
		
	    ConstraintViolationException constraintViolationException  =  getConstraintViolationException(person);
		
	    if (constraintViolationException != null) {
	    	return constraintViolationException;
	    }

	    return null;
	}

	private ConstraintViolationException getConstraintViolationException(final Object person) {

	    Set<ConstraintViolation<Object>> constraintViolations = ValidatorFactoryHelper.getValidator().validate(person);

	    if (constraintViolations.size() > 0 ) {
	    	ConstraintViolationException  constraintViolationException = new ConstraintViolationException(constraintViolations);
	    	return constraintViolationException;
	    }
	    return null;
	}
}