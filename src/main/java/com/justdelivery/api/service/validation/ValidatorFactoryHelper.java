package com.justdelivery.api.service.validation;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class ValidatorFactoryHelper {

	private static volatile ValidatorFactory validatorFactory;

	protected static synchronized ValidatorFactory getInstance() {
		if (validatorFactory == null) {

			if (validatorFactory == null) {
				validatorFactory = Validation.buildDefaultValidatorFactory();
			}

		}
		return validatorFactory;
	}

	public static Validator getValidator() {
		return getInstance().getValidator();
	}
}
