package com.justdelivery.api.exception;

import java.util.List;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.server.validation.ValidationError;
import org.glassfish.jersey.server.validation.internal.ValidationHelper;
/**
 * ValidationExceptionMapper
 *
 * @author Sandun Lewke Bandara
 *
 */
@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    @Override
    public Response toResponse(ConstraintViolationException exception) {

			return Response.status(Status.BAD_REQUEST).entity(
					new GenericEntity<List<ValidationError>>(ValidationHelper.constraintViolationToValidationErrors(exception),
							new GenericType<List<ValidationError>>() {}.getType())).build();
    }
}