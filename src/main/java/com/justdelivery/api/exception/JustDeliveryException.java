package com.justdelivery.api.exception;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonAutoDetect(fieldVisibility=Visibility.PROTECTED_AND_PUBLIC, getterVisibility=Visibility.NONE, setterVisibility=Visibility.NONE)
@JsonInclude(Include.NON_EMPTY)
public class JustDeliveryException extends Exception{

	private static final long serialVersionUID = 856060147469630032L;
	
	private ErrorResponse errorResponse;

	public JustDeliveryException(){
	    
	}
	public JustDeliveryException(ErrorResponse error) {
	    this.errorResponse = error;
	}

	public ErrorResponse getErrorResponse() {
	    return errorResponse;
	}

	public void setErrorResponse(ErrorResponse errorResponse) {
	    this.errorResponse = errorResponse;
	}

}
