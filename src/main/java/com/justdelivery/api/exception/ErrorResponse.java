package com.justdelivery.api.exception;

import java.util.Map;

public class ErrorResponse {
    
    private ErrorCode errorCode;
    
    Map<String, String> fieldErrors;

    public ErrorResponse(){
	
    }
    public ErrorResponse(ErrorCode errorCode, Map<String, String> fieldErrors) {
	this.errorCode = errorCode;
	this.fieldErrors = fieldErrors;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public Map<String, String> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(Map<String, String> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }
    
}
