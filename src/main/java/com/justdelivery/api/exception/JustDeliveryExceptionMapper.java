package com.justdelivery.api.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
 
@Provider
public class JustDeliveryExceptionMapper implements ExceptionMapper<JustDeliveryException> {

    @Override
    public Response toResponse(JustDeliveryException exception) {
	//  GenericEntity<List<ErrorCode> > errorCodes = new GenericEntity<List<ErrorCode>>(exception.getErrorResponse()){};
	return Response.status(Status.BAD_REQUEST).entity(exception).build();
    }
}