package com.justdelivery.api.exception;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * These error codes are defined for the application to return in the event of a
 * failure of any kind.
 * </p>
 * <p>
 * The int code can be used by a client to uniquely identify a type of error.
 * </p>
 */
public enum ErrorCode{

	EMAIL_ALREADY_EXISTS(1000, "email already registered on our service");
 
	private int code;
	private String msg;
	private static Map<String, ErrorCode> errorCodes = new HashMap<String, ErrorCode>();
	
	static{
		for(ErrorCode ec : ErrorCode.values()){
			errorCodes.put(ec.getMsg(), ec);
		}
	}
	
	public static ErrorCode lookup(String msg) {
		return errorCodes.get(msg);
	}

	private ErrorCode(int code, String msg){
		this.code = code;
		this.msg = msg;
	}

	public int getCode(){
		return code;
	}

	public String getMsg(){
		return msg;
	}
}
