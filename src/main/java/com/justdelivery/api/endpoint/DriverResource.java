package com.justdelivery.api.endpoint;

import javax.validation.Valid;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.justdelivery.api.exception.JustDeliveryException;
import com.justdelivery.api.model.Driver;
import com.justdelivery.api.service.PersonRegistrationService;


@Path("/driver")
@Component
public class DriverResource  {

	private PersonRegistrationService<Driver> personRegistrationService;

	private UriInfo uriInfo;
	
	@PUT
	@Path("/signup/")
	@ResponseBody
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response signUp(@RequestBody @Valid final Driver person) throws JustDeliveryException{

		String personId = personRegistrationService.register(person);

		return Response.created(uriInfo.getAbsolutePathBuilder().path("{id}").build(personId)).build();
	}

	@Autowired
	public void setPersonRegistrationService(PersonRegistrationService<Driver> personRegistrationService) {
	    this.personRegistrationService = personRegistrationService;
	}
	
	@Context
	public void setUriInfo(UriInfo uriInfo) {
		this.uriInfo = uriInfo;
	}
}
