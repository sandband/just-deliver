package com.justdelivery.api.endpoint;

import java.util.List;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.server.validation.ValidationError;
import org.glassfish.jersey.server.validation.internal.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.justdelivery.api.exception.JustDeliveryException;
import com.justdelivery.api.model.Person;
import com.justdelivery.api.service.PersonRegistrationService;
import com.justdelivery.api.service.validation.PersonValidationService;

 
@Deprecated
@Path("/person")
@Component
public class PersonResource {

    	private PersonRegistrationService<Person> personRegistrationService;
	
	private PersonValidationService personValidationService;
 
	private UriInfo uriInfo;
	
	@PUT
	@Path("/signup/")
	@ResponseBody
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response signUp(@RequestBody @Valid final Person person) throws JustDeliveryException{
	    
//	    ConstraintViolationException validationException =   personValidationService.validatePersonSignUpRequest(person);
//		
//	    if(validationException != null){
//			return Response.status(HttpStatus.BAD_REQUEST.value()).entity(
//					new GenericEntity<List<ValidationError>>(ValidationHelper.constraintViolationToValidationErrors(validationException),
//							new GenericType<List<ValidationError>>() {}.getType())).build();
//	    }

		String personId = personRegistrationService.register(person);
		return Response.created(uriInfo.getAbsolutePathBuilder().path("{id}").build(personId)).build();
	}
	
	
	@Autowired
	public void setPersonValidationService(PersonValidationService personValidationService) {
		this.personValidationService = personValidationService;
	}
	
	@Autowired
	public void setPersonRegistrationService(PersonRegistrationService<Person> personRegistrationService) {
	    this.personRegistrationService = personRegistrationService;
	}
	
	@Context
	public void setUriInfo(UriInfo uriInfo) {
		this.uriInfo = uriInfo;
	}
}
